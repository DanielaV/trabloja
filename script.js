function criaTarefa(tableId, inputId) {
    var tarefa = document.getElementById(inputId).value;
    if (tarefa !== "") {
      var table = document.getElementById(tableId);
      var row = table.insertRow(-1);
      var cell1 = row.insertCell(0);
      var cell2 = row.insertCell(1);
      cell1.innerHTML = tarefa;
      cell2.innerHTML = '<button class="delete-button button-3d" onclick="removerTarefa(this)">Remover</button>';
      document.getElementById(inputId).value = "";
    }else {
      alert("Você não digitou uma tarefa");
    }
  }
  
  function deletaTarefa(button) {
    var row = button.parentNode.parentNode;
    row.parentNode.removeChild(row);
  }